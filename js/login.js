/*
点击登录按钮
(1)收集表单数据
(2)校验数据 : 非空校验 + 正则校验 
(3)发送ajax
(4)响应成功之后 , 跳转首页
*/
// 点击登录按钮
document.querySelector('#btn-login').addEventListener('click',async function(){
    // (1)收集表单数据
    const username = document.querySelector('[name="username"]').value
    const password = document.querySelector('[name="password"]').value
    // console.log(username,password);
    // (2)校验数据 : 非空校验 + 正则校验
    if(username === '' || password === ''){
        return Toast.fail("用户名和密码不能为空")
    }else if(username.length < 2 || username.length > 30){
        return Toast.fail("用户名必须为2~30位")
    }else if(password.length < 6 || password.length > 30){
        return Toast.fail("密码必须为6~30位")
    }
    // (3)发送ajax
    const {data} = await axios.post('/login',{username,password})
    console.log(data);
    // 存token
    localStorage.setItem('token',data.data.token)
    // (4)响应成功之后 , 跳转首页
    Toast.success('登录成功')
    location.href = "./index.html"
})
