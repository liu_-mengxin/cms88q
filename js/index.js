/*首页业务
    1.页面一加载,ajax获取统计数据
    2.把统计的数据渲染到图表
*/ 
// 1.页面一加载,ajax获取统计数据
// load事件 页面DOM + 外部资源
window.addEventListener('load',async function(){
    // (1)获取数据
    const {data} = await axios.get('/dashboard')
    console.log(data);
    // (2)渲染图表
    setOveryview(data.data.overview)  //概览
    setLine(data.data.year)  //平均薪资
    setPie(data.data.salaryData)  //薪资分布
    setBar(data.data.groupData)  //柱状图
    setGender(data.data.salaryData)  //男女分布
    setMap(data.data.provinceData)   //男女分布
})

/*
    2.1 数据概览
    2.2 折线图 : 月份平均薪资
    2.3 饼图 : 薪资分布
    2.4 柱状图 : 班级每组薪资
    2.5 饼图 : 男女薪资分布
    2.6 地图 : 省数据
*/ 
// 2.1 数据概览
const setOveryview = data => {
    console.log(data);
    console.log(Object.keys(data));
    Object.keys(data).forEach(item =>{
        document.querySelector(`[name=${item}]`).innerHTML = data[item]

    })
}
// 2.2 折线图 : 月份平均薪资
const setLine = data => {
    console.log(data);
    const xData = data.map(item => item.month)
    const yData = data.map(item => item.salary)
    // 3.1基于准备好的dom 初始化echarts实例
    let myChart = echarts.init(document.querySelector('#line'));
    //3.2  图表样式 : 指定图表的配置项和数据 
    const option = {
        // 1.标题
        title: {
            text: '2021全学科薪资走势',
            top: 15,
            left: 10,
            textStyle: {
                color: '#6d767e',
                fontSize: 16
            }
        },
        // 2.鼠标移入提示
        tooltip: {
            trigger: 'axis',
            position: function (pt) {
                return [pt[0], '10%']
            }
        },
        // 3.x轴数据: 必须
        xAxis: {
            type: 'category', // 类别
            data: xData
        },
        // 4.y轴数据：必须
        yAxis: {
            type: 'value',
            boundaryGap: [0, '50%'] // Y轴留白，会影响最大值，最小值
        },
        // 5.线条样式
        series: [
            {
                type: 'line',
                smooth: true, //设置折线圆角
                symbolSize: 10, // 小圆圈大小
                data: yData,
                //线条颜色
                lineStyle: {
                    width: 6,
                    color: '#4281ef'
                },
                //内容区域颜色
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        {
                            offset: 0, color: '#499FEE' // 0% 处的颜色
                        }, {
                            offset: 0.8, color: 'rgba(255, 255, 255, 0.2)' // 80% 处的颜色
                        }, {
                            offset: 1, color: 'rgba(255, 255, 255, 0)' // 100% 处的颜色
                        }
                    ])
                },
            }
        ]
    }

    // 3.3使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
// 2.3 饼图 : 薪资分布
const setPie = data => {
    console.log(data);
    const newArr = data.map(item => {
        return {
            name:item.label,
            value:item.g_count + item.b_count
        }
    })

    // 3.1 基于准备好的dom ,初始化echarts实例
    const myChart = echarts.init(document.querySelector('#salary'));

    // 3.2 图表样式 ,指定图表的配置项和数据
    const option = {
        title: {
            text: "班级薪资分布",
            left: 10,
            top: 10
        },
        tooltip: {
          trigger: 'item'
        },
        legend: {
            bottom: '2%',
            left: 'center'
        },
        color: ["#fda224", "#5097ff", "#3abcfa", "#34d39a"],
        series: [
            {
                // name: 'Access From',
                type: 'pie',
                radius: ['75%', '60%'],
                avoidLabelOverlap: false,
                itemStyle: {
                    borderRadius: 20,
                    borderColor: '#fff',
                    borderWidth: 2
                },
                label: {
                    show: false,
                    position: 'center'
                },
                // emphasis: {
                //     label: {
                //       show: true,
                //       fontSize: 40,
                //       fontWeight: 'bold'
                //     }
                //   },
                labelLine: {
                    show: false
                },
                data: newArr
            }
        ]
    }
    // { value: 300, name: 'Video Ads' }
    //   3.3 使用刚指定的配置项和数据显示图表
    myChart.setOption(option);
}
// 2.4 柱状图 : 班级每组薪资
const setBar = data => {
    console.log(data);
    //  默认取第一个小组数据
    const nameArr = data[1].map(item =>item.name)
    const hope_salaryArr = data[1].map(item =>item.hope_salary)
    const salaryArr = data[1].map(item =>item.salary)

     // 3.1 基于准备好的dom ,初始化echarts实例
     const myChart = echarts.init(document.querySelector('#lines'));

     // 3.2 图表样式 ,指定图表的配置项和数据
     const option = {
       // 0.标题
       title: {
         text: "班级每组薪资"
       },
       // 1.鼠标移入提示
       tooltip: {
         trigger: 'axis',
         axisPointer: {
           type: 'shadow'
         }
       },
       // 2.顶部小圈圈提示
       // legend: {},
       // 3.内边距相当于padding
       grid: {
         left: '3%',
         right: '4%',
         bottom: '3%',
         containLabel: true
       },
       // 4.X轴
       xAxis: [
         {
           type: 'category',
           data: nameArr
         }
       ],
       // 5.Y轴
       yAxis: [
         {
           type: 'value'
         }
       ],
       // 6.图标内容区域
       series: [
         {
           name: '期望薪资',
           type: 'bar',
           // 鼠标移入隐藏其他柱子,自己高亮
           emphasis: {
             focus: 'series'
           },
           // 柱子样式
           itemStyle: {
             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
               {
                 offset: 0,
                 color: '#00d9a4'
               },
               {
                 offset: 1,
                 color: '#b6f4e4'
               }
             ])
           },
           data: hope_salaryArr
         },
         {
           name: '实际薪资',
           type: 'bar',
           // 堆叠柱子:相同的stack会堆叠在一起
           stack: 'Ad',
           emphasis: {
             focus: 'series'
           },
           // 柱子样式
           itemStyle: {
             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
               {
                 offset: 0,
                 color: '#00a9f1'
               },
               {
                 offset: 1,
                 color: '#98e0f9'
               }
             ])
           },
           data: salaryArr
         },
       ]
     };
     //   3.3 使用刚指定的配置项和数据显示图表
     myChart.setOption(option);

    //  点击小组按钮,注册委托事件
    document.querySelector('#btns').addEventListener('click',function(e){
        if(e.target.tagName === 'BUTTON'){
            // (0)排他 ,干掉选中的类名,设置自己的类名
            document.querySelector('.btn-blue').classList.remove('btn-blue')
            e.target.classList.add('btn-blue')
            // (1)获取小组编号,根据小组生成图表数据
            console.log(e.target.innerHTML);
            const nameArr = data[e.target.innerHTML].map(item =>item.name)
            const hope_salaryArr = data[e.target.innerHTML].map(item =>item.hope_salary)
            const salaryArr = data[e.target.innerHTML].map(item =>item.salary)
            // (2)修改图表数据
            console.log(option);
            option.xAxis[0].data = nameArr
            option.series[0].data = hope_salaryArr
            option.series[1].data = salaryArr
            // (3)重新渲染图表
            myChart.setOption(option)

        }
    })
}
// 2.5 饼图 : 男女薪资分布
const setGender = data => {
    console.log(data)
    const gData = data.map(item => {
        return {
            name:item.label,
            value:item.g_count
        }
       })
       const bData = data.map(item =>{
        return {
            name:item.label,
            value:item.b_count
        }
       })
    
        // 3.1 基于准备好的dom ,初始化echarts实例
        const myChart = echarts.init(document.querySelector('#gender'));
    
        // 3.2 图表样式 ,指定图表的配置项和数据
        const option = {
          title: [
            {
              text: "男女薪资分布",
              left:20,
              top:20
    
            },
            {
              text: "男生",
              left: "center",
              top: "50%",
              textStyle: {
                fontSize: 20,
                fontWeight: 400
              }
    
            },
            {
              text: "女生",
              left: "center",
              top: "93%",
              textStyle: {
                fontSize: 20,
                fontWeight: 400
              }
    
            },
          ],
          // tooltip: {
          //   trigger: 'item'
          // },
          // legend: {
          //   top: '5%',
          //   left: 'center'
          // },
          color: ["#fda224", "#5097ff", "#3abcfa", "#34d39a"],
          series: [
            {
              // name: 'Access From',
              type: 'pie',
              radius: ['20%', '30%'],
              center: ['50%', '25%'],
              avoidLabelOverlap: false,
              // label: {
              //   show: false,
              //   position: 'center'
              // },
              // emphasis: {
              //   label: {
              //     show: true,
              //     fontSize: 40,
              //     fontWeight: 'bold'
              //   }
              // },
              // labelLine: {
              //   show: false
              // },
              data: gData
            },
            {
              // name: 'Access From',
              type: 'pie',
              radius: ['20%', '30%'],
              center: ['50%', '75%'],
              avoidLabelOverlap: false,
              // label: {
              //   show: false,
              //   position: 'center'
              // },
              // emphasis: {
              //   label: {
              //     show: true,
              //     fontSize: 40,
              //     fontWeight: 'bold'
              //   }
              // },
              // labelLine: {
              //   show: false
              // },
              data: bData
            },
          ]
        }
        // { value: 300, name: 'Video Ads' }
        //   3.3 使用刚指定的配置项和数据显示图表
        myChart.setOption(option);
}
// 2.6 地图 : 省数据
const setMap = data => {
    console.log(data);
     // 3.1 基于准备好的dom，初始化echarts实例
     let myChart = echarts.init(document.querySelector('#map'));

     // 3.2 指定图表的配置项和数据
     const dataList = data
 
     // 数据设置
     dataList.forEach((item) => {
         // 数据里名字和上面的名字有点不太一样, 需要把多余的文字去掉(替换成空字符串)
         item.name = item.name.replace(/省|回族自治区|吾尔自治区|壮族自治区|特别行政区|自治区/g, '') 
     })
 
     let option = {
         title: {
             text: '籍贯分布',
             top: 10,
             left: 10,
             textStyle: {
                 fontSize: 16,
             },
         },
         tooltip: {
             trigger: 'item',
             formatter: '{b}: {c} 位学员',
             borderColor: 'transparent',
             backgroundColor: 'rgba(0,0,0,0.5)',
             textStyle: {
                 color: '#fff',
             },
         },
         visualMap: {
             min: 0,
             max: 6,
             left: 'left',
             bottom: '20',
             text: ['6', '0'],
             inRange: {
                 color: ['#ffffff', '#0075F0'],
             },
             show: true,
             left: 40,
         },
         geo: { // 地理坐标系组件
             map: 'china',
             roam: false,
             zoom: 1.0,
             label: {
                 normal: {
                     show: true,
                     fontSize: '10',
                     color: 'rgba(0,0,0,0.7)',
                 },
             },
             itemStyle: {
                 normal: {
                     borderColor: 'rgba(0, 0, 0, 0.2)',
                     color: '#e0ffff',
                 },
                 emphasis: {
                     areaColor: '#34D39A',
                     shadowOffsetX: 0,
                     shadowOffsetY: 0,
                     shadowBlur: 20,
                     borderWidth: 0,
                     shadowColor: 'rgba(0, 0, 0, 0.5)',
                 },
             },
         },
         series: [
             {
                 name: '籍贯分布',
                 type: 'map',
                 geoIndex: 0,
                 data: dataList,
             },
         ],
     }
 
     // 3.3使用刚指定的配置项和数据显示图表。
     myChart.setOption(option);
}