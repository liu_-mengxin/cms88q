/*
    1.页面一加载 ,发送ajax请求
    2.渲染列表
*/
// 封装函数
const getStudentList = async () => {
    // 发送ajax请求 获取学生列表
    const { data } = await axios.get('/students')
    console.log(data);
    // 渲染列表
    document.querySelector('.list').innerHTML = data.data.map(item => `
        <tr>
            <td>${item.name}</td>
            <td>${item.age}</td>
            <td>${item.gender ? '女' : '男'}</td>
            <td>${item.group}</td>
            <td>${item.hope_salary}</td>
            <td>${item.salary}</td>
            <td>${item.province + item.city + item.area}</td>
            <td>
            <a href="javascript:;" class="text-success mr-3"><i data-id="${item.id}" class="bi bi-pen"></i></a>
            <a href="javascript:;" class="text-danger"><i  data-id="${item.id}" class="bi bi-trash"></i></a>
            </td>
        </tr>
    `).join('')
}
// 调用函数
getStudentList()

/*2.新增学生
    2.1 点击新增按钮:弹出模态框
    2.2 省市县联动
    2.3 点击确认 : 新增学生
    2.4 点击取消 : 重置表单
*/
// 声明一个变量id
let id = ''
// 2.1 点击新增按钮:弹出模态框
document.querySelector('#openModal').addEventListener('click', function () {
    // 弹出模态框
    new bootstrap.Modal('#modal').show()
    // 渲染列表
    getProvince()
    // 清空id
    id = ''

})

/*2.2
    (1)加载省 : 弹出模态框的时候
    (2)加载市 : 省的change事件
    (3)加载县 : 市的change事件
*/
// 获取下拉菜单
let province = document.querySelector('[name="province"]')
let city = document.querySelector('[name="city"]')
let area = document.querySelector('[name="area"]')
// 省的change事件
province.addEventListener('change', function () {
    // 加载市
    getCity()

})
// 市的change事件
city.addEventListener('change', function () {
    // 加载县
    getArea()
})
// (1)加载省 : 弹出模态框的时候
const getProvince = async () => {
    // 获取省数据
    const { data } = await axios.get('/api/province')
    console.log(data);
    // 渲染省列表
    province.innerHTML = `<option value="">--省份--</option>` + data.data.map(item => `<option value="${item}">${item}</option>`)


}
// (2)加载市 : 省的change事件
const getCity = async () => {
    // 获取市数据
    const { data } = await axios.get(`/api/city?pname=${province.value}`)
    console.log(data);
    // 渲染市列表
    city.innerHTML = `<option value="">--城市--</option>` + data.data.map(item => `<option value="${item}">${item}</option>`)
    // 省变化 县也要清空
    area.innerHTML = `<option value="">--地区--</option>`
}
// (3)加载县 : 市的change事件
const getArea = async () => {
    // 获取县数据
    const { data } = await axios.get(`/api/area?pname=${province.value}&cname=${city.value}`)
    console.log(data);
    // 渲染县列表
    area.innerHTML = `<option value="">--地区--</option>` + data.data.map(item => `<option value="${item}">${item}</option>`)

}
// 2.3 点击确认 : 新增学生
document.querySelector('#submit').addEventListener('click', async function () {
    const fd = new FormData(document.querySelector('form'))
    const obj = {}
    fd.forEach((value, key) => {
        obj[key] = value
    })
    obj.age = +obj.age
    obj.gender = +obj.gender
    obj.group = +obj.group
    obj.hope_salary = +obj.hope_salary
    obj.salary = +obj.salary

    if (Object.values(obj).some(item => item === '')) {
        return Toast.fail('输入框不能为空')
    }
    if (id) {
        const { data } = await axios.put(`/students/${id}`, obj)
        console.log(data);
        Toast.success('修改学生成功')
    } else {
        const { data } = await axios.post('/students', obj)
        console.log(data);
        Toast.success('新增学生成功')
    }
    // (4)成功: 刷新学生列表 + 隐藏模态框 + 清空表单
    document.querySelector('.btn-secondary').click()
    getStudentList()

})
// 2.4 点击取消: 重置表单
document.querySelector('.btn-secondary').addEventListener('click', function () {
    document.querySelector('form').reset()
})
/*3.编辑学生
    3.1 点击编辑(事件委托),弹出模态框
    3.2 省市县联动
    3.3 数据回显 当前学生信息显示在模态框
    3.4 完成修改学生

*/
//  3.1 点击编辑(事件委托),弹出模态框
document.querySelector('tbody').addEventListener('click', async function (e) {
    if (e.target.classList.contains('bi-pen')) {
        // 弹出模态框
        new bootstrap.Modal('#modal').show()
        //  3.2 省市县联动
        // 存储id
        id = e.target.dataset.id
        /*3.3 数据回显
            (1)获取学生详情
            (2)渲染学生详情 到模态框
        
        */
        const { data } = await axios.get(`/students/${e.target.dataset.id}`)
        console.log(data);
        //    (2)渲染学生详情 到模态框
        document.querySelector('[name="name"]').value = data.data.name
        document.querySelector('[name="age"]').value = data.data.age
        document.querySelector('[name="group"]').value = data.data.group
        document.querySelector('[name="hope_salary"]').value = data.data.hope_salary
        document.querySelector('[name="salary"]').value = data.data.salary

        document.querySelectorAll('[name="gender"]')[data.data.gender].checked = true
        // 获取省列表
        await getProvince()
        // 渲染学生当前的省
        province.value = data.data.province
        // 获取市
        await getCity()
        // 渲染学生当前的市
        city.value = data.data.city
        // 获取县
        await getArea()
        // 渲染学生当前的县
        area.value = data.data.area
    }else if(e.target.classList.contains('bi-trash')){
        // 发送删除ajax
        const {data} = await axios.delete(`/students/${e.target.dataset.id}`)
        // 删除成功,刷新列表
        Toast.success('删除成功')
        getStudentList()
    }
})



