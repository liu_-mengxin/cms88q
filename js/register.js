/*
点击注册按钮
(1)收集表单数据
(2)校验数据 : 非空校验 + 正则校验 
(3)发送ajax
(4)响应成功之后 , 跳转登录页
*/

// 点击注册按钮
document.querySelector('#btn-register').addEventListener('click',async function(){
    // (1)收集表单数据
    const username = document.querySelector('[name="username"]').value
    const password = document.querySelector('[name="password"]').value
    console.log(username,password);
    // (2)校验数据 : 非空校验 + 正则校验
    if(username ===  '' || password === ''){
        return Toast.fail("用户名和密码不能为空")
    }else if(username.length < 2 || username.length > 30){
        return Toast.fail("用户名为2~30位")
    }else if(password.length < 6 || password.length > 30){
        return Toast.fail("密码为6~30位")
    }
    // (3)发送ajax
    const {data} = await axios.post('/register',{username,password})
    console.log(data);
    // (4)响应成功之后 , 跳转登录页
    location.href = './login.html'
})