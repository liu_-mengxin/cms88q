// 设置基地址
axios.defaults.baseURL = 'http://ajax-api.itheima.net'

/*拦截器工作流程
    1.页面发送ajax请求
    2.执行 请求拦截器
    3.请求发送到服务器
    4.服务器响应数据之后
    5.执行响应拦截器
    6.执行axios的then方法
*/ 


// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    //(1)请求成功 在发送请求之前做些什么
    // 如果localStorage中存了token,就统一给请求头添加token
    if(localStorage.getItem('token')){
        config.headers.Authorization = localStorage.getItem('token')
    }
    return config;//请求报文
  }, function (error) {
    //(2)请求失败 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    //(1)请求成功 对响应数据做点什么
    return response;//响应报文
  }, function (error) {
    //(2)请求失败 对响应错误做点什么
    // a . 统一错误提示
    Toast.fail(error.response.data.message)
    // b. 如果是401状态码 ,说明token没有 或 token过期 ,跳转登录页
    if(error.response.status === 401) {
        location.href = "./login.html"
    }
    return Promise.reject(error);
  });